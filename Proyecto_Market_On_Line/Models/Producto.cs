﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto_Market_On_Line.Models
{
    public class Producto
    {
        [Key]
        public int ID { get; set; }
       [Required]
        public string Codigo { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public int Estado { get; set; }
        [Required]
        public int FechaCreacion { get; set; }
        [Required]
        public int UnidadMedida { get; set; }
        [Required]
        public string ClasificacionproductoID { get; set; }
        [Required]
        public int Precio { get; set; }

    }
}