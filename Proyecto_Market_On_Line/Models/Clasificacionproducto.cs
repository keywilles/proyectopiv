﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto_Market_On_Line.Models
{
    public class Clasificacionproducto
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public int Codigo { get; set; }
        [Required]
        public int Clasificacion { get; set; }
        [Required]
        public int Estado { get; set; }
    }
}