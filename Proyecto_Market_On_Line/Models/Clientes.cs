﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto_Market_On_Line.Models
{
    public class Clientes
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [StringLength (50, ErrorMessage="El nombre debe contener al menos 50 caracteres")]
        public string Nombre { get; set; }
    }
}