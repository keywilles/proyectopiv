﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto_Market_On_Line.Models
{
    public class Ordenes
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string FechaCreacion { get; set; }
        [Required]
        public int ClienteID { get; set; }
    }
}