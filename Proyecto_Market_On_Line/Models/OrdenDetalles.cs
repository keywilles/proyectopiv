﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Proyecto_Market_On_Line.Models
{
    public class OrdenDetalles
    {
        [Key]
        public int ID { get; set; }
        public string OrdenID { get; set; }
        [Required]
        public int ProductoID { get; set; }
        [Required]
        public int Precio { get; set; }
        [Required]
        public int Cantidad { get; set; }
        [Required]
        public int Descuento { get; set; }
        public int Total { get; set; }
    }
}