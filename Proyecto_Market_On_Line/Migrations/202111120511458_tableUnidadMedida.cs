namespace Proyecto_Market_On_Line.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tableUnidadMedida : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UnidadMedidas",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Codigo = c.String(nullable: false),
                        Descripcion = c.String(nullable: false),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UnidadMedidas");
        }
    }
}
