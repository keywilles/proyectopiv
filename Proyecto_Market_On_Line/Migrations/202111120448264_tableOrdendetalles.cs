namespace Proyecto_Market_On_Line.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tableOrdendetalles : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrdenDetalles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        OrdenID = c.String(),
                        ProductoID = c.Int(nullable: false),
                        Precio = c.Int(nullable: false),
                        Cantidad = c.Int(nullable: false),
                        Descuento = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.OrdenDetalles");
        }
    }
}
