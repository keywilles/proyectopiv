﻿namespace Proyecto_Market_On_Line.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Ordenes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FechaCreacion = c.String(nullable: false),
                        ClienteID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Ordenes");
        }
    }
}
