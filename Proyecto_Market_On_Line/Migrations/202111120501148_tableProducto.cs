namespace Proyecto_Market_On_Line.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tableProducto : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Productoes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Codigo = c.String(nullable: false),
                        Nombre = c.String(nullable: false),
                        Estado = c.Int(nullable: false),
                        FechaCreacion = c.Int(nullable: false),
                        UnidadMedida = c.Int(nullable: false),
                        ClasificacionproductoID = c.String(nullable: false),
                        Precio = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Productoes");
        }
    }
}
