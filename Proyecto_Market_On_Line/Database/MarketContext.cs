﻿using Proyecto_Market_On_Line.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Proyecto_Market_On_Line.Database
{
    public class MarketContext: DbContext
    {
        public MarketContext(): base("name= Ordenes")
        {

        }
        DbSet<Ordenes> Ordenes { get; set; }
        DbSet<Clientes> Clientes { get; set; }
        DbSet<OrdenDetalles> OrdenDetalles { get; set; }
        DbSet<Producto> Producto { get; set; }
        DbSet<Clasificacionproducto> Clasificacionproducto { get; set; }
        DbSet<UnidadMedida> UnidadMedida { get; set; }
    }
}